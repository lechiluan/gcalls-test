const mocha = require('mocha');
const chai = require('chai');
const puppeteer = require('puppeteer');

const { expect } = chai;
const assert = require('assert');

describe('GCalls', () => {
    let browser;
    let page;

    beforeEach(async () => {
        global.expect = expect;
        browser = await puppeteer.launch({ headless: false });
        page = await browser.newPage();
        await page.setViewport({
            width: 960,
            height: 1000,
        });
    });

    afterEach(async () => {
        await page.close();
        await browser.close();
    });

    // Test case 1 - Go to register page
    it('Check go to register page', async () => {
        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');
        const title = await page.title();
        expect(title).to.equal('Gcalls - Đăng ký');
    });


    // Test case 2 - Enter correct call center name and email admin account
    it('Enter correct call center name and email admin account', async () => {
        callCenterName = 'call1';
        email = 'luan.le@gcalls.co';

        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');
        const title = await page.title();

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Fill in the email
        await page.type('#email', email);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear
        await page.waitForSelector('.modal-content');

        // Get the text of the success message
        const successMessage = await page.$eval('.modal-content', el => el.innerText);

        // Assert on the success message content
        expect(successMessage).to.include('Kiểm tra email trong vòng 48h để thực hiện các bước tiếp theo của thủ tục đăng ký tài khoản');
    });

    // Test case 3 - Enter empty call center name and correct email admin account
    it('Enter empty call center name and correct email admin account', async () => {
        callCenterName = '';
        email = 'luan.le@gcalls.co';

        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');
        const title = await page.title();

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Fill in the email
        await page.type('#email', email);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="emailError"
        await page.waitForSelector('#tenantError');
        // Get the text of the success message
        const successMessage = await page.$eval('#tenantError', el => el.innerText);

        // Assert on the success message content
        expect(successMessage).to.include('Link tổng đài không được rỗng');
    });
    // Test case 4 - Enter correct call center name and empty email admin account
    it('Enter correct call center name and empty email admin account', async () => {
        callCenterName = 'call1';
        email = '';

        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');
        const title = await page.title();

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Fill in the email
        await page.type('#email', email);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="emailError"
        await page.waitForSelector('#emailError');
        // Get the text of the success message
        const successMessage = await page.$eval('#emailError', el => el.innerText);

        // Assert on the success message content
        expect(successMessage).to.include('Email Admin không được rỗng');
    });

    // Test case 5 - Enter call center name is exist and correct email admin account
    // Error message: Tổng đài đã tồn tạ -> Thiếu chữ i ở cuối từ "tạ" -> Tổng đài đã tồn tại
    it('Enter call center name is exist and correct email admin account', async () => {
        callCenterName = 'test';
        email = 'luan.le@gcalls.co';

        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');

        // Fill in the email
        await page.type('#email', email);

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="emailError"
        await page.waitForSelector('div#tenantError.error');
        // Get the text of the success message from id="tenantError"
        const successMessage = await page.$eval('div#tenantError.error', el => el.innerText);
        // Assert on the success message content
        expect(successMessage).to.equal('Tổng đài đã tồn tạ');
    });

    // Test case 6 - Enter correct call center name and email admin account is invalid
    it('Enter correct call center name and email admin account is invalid', async () => {
        callCenterName = 'calltest';
        email = 'luan.le';

        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');

        // Fill in the email
        await page.type('#email', email);

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');     

        // Wait for the success message to appear id="emailError"
        await page.waitForSelector('div#emailError.error');
        // Get the text of the success message from id="emailError"

        const successMessage = await page.$eval('div#emailError.error', el => el.innerText);

        // Assert on the success message content
        expect(successMessage).to.equal('Email Admin không đúng định dạng');
    }); 

    // Test case 7 - Go to login page from register page
    // Fail - It don't go to login page  because it link is incorect
    it('Go to login page from register page', async () => {
        await page.goto('https://app.gcalls.co/g/login');

        const logoLink = ".logo";
        await page.evaluate((logoLink) => {
            // Find the element by class name
            const link = document.querySelector(logoLink);
            // Trigger a click event on the element
            link.click();
        }, logoLink);
        await page.waitForSelector('button[class="swal-button swal-button--confirm"]');
        await page.click('button[class="swal-button swal-button--confirm"]');

        await page.waitForSelector('a[href="/g/"]');

        await page.click('a[href="/g/"]');

        const title = await page.title();
        expect(title).to.equal('Gcalls - Đăng nhập');
    });     

    // Test case 8 - Go to set password page form url sent to email
    // URL: https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8
    it('Go to set password page form url sent to email', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        const title = await page.title();
        expect(title).to.equal('Gcalls - Cài đặt mật khẩu');
    });

    // Test case 9 - Enter password and confirm password is empty
    it('Enter password and confirm password is empty', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        password = '';
        confirmPassword = '';

        // Fill in the password id="password"
        await page.type('#password', password);

        // Fill in the confirm password id="passwordConfirm"
        await page.type('#passwordConfirm', confirmPassword);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="passwordError"

        await page.waitForSelector('#passwordError');
        // Wait for the success message to appear id="passwordConfirmError"
        await page.waitForSelector('#passwordConfirmError');
        // Get the text of the success message from id="passwordError"

        const successMessagePassword = await page.$eval('#passwordError', el => el.innerText);
        const successMessagePasswordConfirm = await page.$eval('#passwordConfirmError', el => el.innerText);

        // Assert on the success message content
        expect(successMessagePassword).to.equal('Mật khẩu không được rỗng');
        expect(successMessagePasswordConfirm).to.equal('Nhập lại mật khẩu không được rỗng');
    });

    // Test case 10 - Enter password and confirm password is not match
    it('Enter password and confirm password is not match', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        password = 'admin@123';
        confirmPassword = 'admin@456';

        // Fill in the password id="password"
        await page.type('#password', password);

        // Fill in the confirm password id="passwordConfirm"
        await page.type('#passwordConfirm', confirmPassword);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="passwordError"

        await page.waitForSelector('#passwordError');
        // Wait for the success message to appear id="passwordConfirmError"
        await page.waitForSelector('#passwordConfirmError');
        // Get the text of the success message from id="passwordError"

        const successMessagePasswordConfirm = await page.$eval('#passwordConfirmError', el => el.innerText);

        // Assert on the success message content
        expect(successMessagePasswordConfirm).to.equal('Nhập lại mật khẩu không trùng khớp');
    });

    // Test case 11 - Enter password and confirm password is less than 6 characters
    it('Enter password and confirm password is less than 6 characters', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        password = 'admin';
        confirmPassword = 'admin';

        // Fill in the password id="password"
        await page.type('#password', password);

        // Fill in the confirm password id="passwordConfirm"
        await page.type('#passwordConfirm', confirmPassword);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="passwordError"

        await page.waitForSelector('#passwordError');
        // Wait for the success message to appear id="passwordConfirmError"
        await page.waitForSelector('#passwordError');
        // Get the text of the success message from id="passwordError"

        const successMessagePassword = await page.$eval('#passwordError', el => el.innerText);

        // Assert on the success message content
        expect(successMessagePassword).to.equal('Mật khẩu không đúng định dạng');
    });

    // Test case 12 - Enter password and confirm password is greater than 32 characters
    it('Enter password and confirm password is greater than 32 characters', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        password = 'passwordpasswordpasswordpasswordpassword';
        confirmPassword = 'passwordpasswordpasswordpasswordpassword';

        // Fill in the password id="password"
        await page.type('#password', password);

        // Fill in the confirm password id="passwordConfirm"
        await page.type('#passwordConfirm', confirmPassword);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="passwordError"

        await page.waitForSelector('#passwordError');
        // Wait for the success message to appear id="passwordConfirmError"
        await page.waitForSelector('#passwordError');
        // Get the text of the success message from id="passwordError"

        const successMessagePassword = await page.$eval('#passwordError', el => el.innerText);

        // Assert on the success message content
        expect(successMessagePassword).to.equal('Mật khẩu không đúng định dạng');
    });

    // Test case 13 - Enter password and confirm password is valid
    it('Enter password and confirm password is valid', async () => {
        await page.goto('https://app.gcalls.co/g/confirm/2168953b1af93bfdbb87d03068d9350d0e41539607f206a848448cd6bac9e3dfcd7672e16560bb530dbd71479c27cd723acb8');

        password = 'admin@123';
        confirmPassword = 'admin@123';

        // Fill in the password id="password"
        await page.type('#password', password);

        // Fill in the confirm password id="passwordConfirm"
        await page.type('#passwordConfirm', confirmPassword);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for the success message to appear id="passwordError"

        await page.title();

        // Assert on the success message content
        expect(page.title()).to.equal('Gcalls - Đăng nhập');
    });
});