const mocha = require('mocha');
const chai = require('chai');
const puppeteer = require('puppeteer');

const { expect } = chai;
const assert = require('assert');

describe('GCalls - Forgot Call Center Functions', () => {
    let browser;
    let page;

    beforeEach(async () => {
        global.expect = expect;
        browser = await puppeteer.launch({ headless: false });
        page = await browser.newPage();
        await page.setViewport({
            width: 960,
            height: 1000,
        });
    });

    afterEach(async () => {
        await page.close();
        await browser.close();
    });

    // Test case 1 - Check go to forgot call center name
    it('Check go to forgot call center name', async () => {
        await page.goto('https://app.gcalls.co/g/login');

        try {
            await page.click('a[href="/g/forgetcallcentername"]');
            const title = await page.title();
            expect(title).to.equal('Gcalls - Bạn không nhớ tên tổng đài? Bấm vào đây!');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Gcalls - Bạn không nhớ tên tổng đài? Bấm vào đây!');
        }
    });

    // Test case 2 - Enter correct email
    it('Enter correct email', async () => {
        email = 'luan.le@gcalls.co';

        await page.goto('https://app.gcalls.co/g/login');

        try {
            await page.click('a[href="/g/forgetcallcentername"]');
            await page.waitForNavigation();

            await page.type('#email', email);
            await page.click('button[type="submit"]');
            await page.waitForNavigation();

            const title = await page.title();
            expect(title).to.equal('Gcalls - Bạn không nhớ tên tổng đài? Bấm vào đây!');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Gcalls - Bạn không nhớ tên tổng đài? Bấm vào đây!');
        }
    });

    // Test case 3 - Enter incorrect email
    it('Enter incorrect email', async () => {
        email = 'test';
        await page.goto('https://app.gcalls.co/g/login');

        await page.click('a[href="/g/forgetcallcentername"]');

        await page.type('#email', email);

        await page.click('button[type="submit"]');

        await page.waitForFunction('document.querySelector("#emailError").innerHTML != ""');

        expect(await page.$eval('#emailError', e => e.innerHTML)).to.equal('Email không đúng định dạng');
    });

    // Test case 4 - Enter email not exist
    it('Enter email not exist', async () => {
        email = 'luan@gcalls.co';
        await page.goto('https://app.gcalls.co/g/login');

        await page.click('a[href="/g/forgetcallcentername"]');

        await page.type('#email', email);

        await page.click('button[type="submit"]');

        try {
            // Wait for the warning message to appear
            await page.waitForSelector('.modal-body');

            // Check if the warning message is displayed
            const warningMessage = await page.$eval('.modal-body', e => e.innerText);
            expect(warningMessage).to.equal('Email chưa được đăng ký\nVui lòng đăng ký lại để tạo tài khoản mới');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected warning message to be displayed');

        }
    });

    // Test case 5 - Link to sign in page -> Fail (bug) because the link is not correct
    it('Link to sign in page', async () => {
        await page.goto('https://app.gcalls.co/g/login');
        await page.click('a[href="/g/forgetcallcentername"]');
        try {
        await page.click('a[href="/"'); 
        expect(await page.title()).to.equal('Gcalls - Đăng nhập');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Gcalls - Đăng nhập');
        }
    });
}); 
