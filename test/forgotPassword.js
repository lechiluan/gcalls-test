const mocha = require('mocha');
const chai = require('chai');
const puppeteer = require('puppeteer');

const { expect } = chai;
const assert = require('assert');

describe('GCalls', () => {
    let browser;
    let page;

    beforeEach(async () => {
        global.expect = expect;
        browser = await puppeteer.launch({ headless: false });
        page = await browser.newPage();
        await page.setViewport({
            width: 960,
            height: 1000,
        });
    });

    afterEach(async () => {
        await page.close();
        await browser.close();
    });

    // Test case 1 - Go to forgot password page
    it('Check go to forgot password page', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Click on the a tag with tabindex = 5
        await page.click('a[tabindex="5"]');

        try {
            // expect h1 with id = kc-page-title to be "Forgot Your Password?"
            const title = await page.$eval('#kc-page-title', el => el.innerText);
            expect(title).to.equal('Forgot Your Password?');
        } catch (error) {
            assert.fail(null, null, 'Test failed: Expected to navigate to the reset password page');
        }
    });

    // Test case 2 - Reset password with valid email
    it('Check reset password with valid email', async () => {
        callCenterName = 'gcallsintern';
        email = 'luan.le@gcalls.co';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Click on the a tag with tabindex = 5
        await page.click('a[tabindex="5"]');

        // Fill in the email field
        await page.type('input[id="username"]', email);

        // Submit the form by clicking the input type="submit"
        await page.click('input[type="submit"]');

        // Wait for navigation or any other relevant event

        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        try {
            const message = await page.$eval('.pf-c-alert__title', el => el.innerText);
            expect(message).to.equal('You should receive an email shortly with further instructions.');
        } catch (error) {
            assert.fail(null, null, 'Test failed: Expected to display message "You should receive an email shortly with further instructions."');
        }
    });

    // Test case 3 - Reset password with invalid email - Fail(Bug) It check email format and not check email exist in database
    it('Check reset password with invalid email', async () => {
        callCenterName = 'gcallsintern';
        email = 'test';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Click on the a tag with tabindex = 5
        await page.click('a[tabindex="5"]');

        // Fill in the email field
        await page.type('input[id="username"]', email);

        // Submit the form by clicking the input type="submit"
        await page.click('input[type="submit"]');

        // Wait for navigation or any other relevant event

        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        try {
            const message = await page.$eval('.pf-c-alert__title', el => el.innerText);
            expect(message).to.equal('Invalid email address.');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected to display message "Invalid email address."');
        }
    });

    // Test case 4 - Reset password with empty email
    it('Check reset password with empty email', async () => {
        callCenterName = 'gcallsintern';
        email = '';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);
        await page.waitForNavigation();

        // Click on the a tag with tabindex = 5
        await page.click('a[tabindex="5"]');

        // Fill in the email field
        await page.type('input[id="username"]', email);

        // Submit the form by clicking the input type="submit"
        await page.click('input[type="submit"]');

        // Wait for the span element to be displayed
        await page.waitForSelector('#input-error-username');

        // Get the inner text of the span element

        try {
            const errorMessage = await page.$eval('#input-error-username', e => e.innerText);
            await page.screenshot({ path: 'test/forgotPassword.png' });
            // Expect the error message to match the expected text
            expect(errorMessage).to.equal('Please specify username.');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected to display message "Please specify username."');
        }
    });
});