const mocha = require('mocha');
const chai = require('chai');
const puppeteer = require('puppeteer');

const { expect } = chai;
const assert = require('assert');

describe('GCalls - Login Functions', () => {
    let browser;
    let page;

    beforeEach(async () => {
        global.expect = expect;
        browser = await puppeteer.launch({ headless: false });
        page = await browser.newPage();
        await page.setViewport({
            width: 960,
            height: 1000,
        });
    });

    afterEach(async () => {
        await page.close();
        await browser.close();
    });

    // Test case 1 - Go to sign in page
    it('Check go to sign in page', async () => {
        try {
            await page.goto('https://app.gcalls.co/g/login');
            const title = await page.title();
            expect(title).to.equal('Gcalls - Đăng nhập');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Gcalls - Đăng nhập');
        }
    });

    // Test case 2 - Enter correct call center name
    it('Enter correct call center name ', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the call center name
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        try {
            // Wait for navigation or any other relevant event
            await page.waitForNavigation();

            // Assert on the URL
            const title = await page.title();
            expect(title).to.equal('Sign in to ' + callCenterName)
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Sign in to ' + callCenterName);
        }
    });

    // Test case 3 - Enter incorrect call center name
    it('Enter incorrect call center name', async () => {
        const callCenterName = 'gcallsintern1';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');
        try {
            // Wait for function to evaluate
            await page.waitForFunction('document.querySelector("#tenantError").innerHTML != ""');
            expect(await page.$eval('#tenantError', e => e.innerHTML)).to.equal('Không tìm thấy tổng đài');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Không tìm thấy tổng đài');
        }
    });

    // Test case 4 - Enter empty call center name
    it('Enter empty call center name', async () => {
        const callCenterName = '';
        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');
        try {
            // Wait for function to evaluate
            await page.waitForFunction('document.querySelector("#tenantError").innerHTML != ""');
            expect(await page.$eval('#tenantError', e => e.innerHTML)).to.equal('Tên tổng đài không được rỗng');
        }
        catch (error) {
            assert.fail(null, null, 'Test failed: Expected title to be ' + 'Không tìm thấy tổng đài');
        }
    });

    // Test case 5 - Enter correct username and password
    it('Enter correct username and password', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Fill in the username and password fields
        await page.type('#username', 'luan.le@gcalls.co');
        await page.type('#password', '@lechiluan@');

        // Submit the form by clicking the input with id = "kc-login"
        await page.click('#kc-login');

        // Wait for navigation or any other relevant event
        await page.waitForNavigation();

        // Assert on the URL
        const title = await page.title();
        expect(title).to.equal('Gcalls - Trang chủ');
    });

    // Test case 6 - Enter incorrect username and password
    it('Enter incorrect username and password', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Fill in the username and password fields
        await page.type('#username', 'luan.le@gcalls.co');
        await page.type('#password', '@admin@');

        // Submit the form by clicking the input with id = "kc-login"
        await page.click('#kc-login');

        try {
            // Wait for the error message to appear
            await page.waitForSelector('#input-error');
    
            // Check if the error message is displayed
            const errorMessage = await page.$eval('#input-error', e => e.innerText.trim());
            expect(errorMessage).to.equal('Invalid username or password.');
        } catch (error) {
            assert.fail(null, null, 'Test failed: Expected error message to be displayed');
        }
    });

    // Test case 7 - Enter empty username and password
    it('Enter empty username and password', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Fill in the username and password fields
        await page.type('#username', '');
        await page.type('#password', '');

        // Submit the form by clicking the input with id = "kc-login"
        await page.click('#kc-login');

        try {
            // Wait for the error message to appear
            await page.waitForSelector('#input-error');
    
            // Check if the error message is displayed
            const errorMessage = await page.$eval('#input-error', e => e.innerText.trim());
            expect(errorMessage).to.equal('Invalid username or password.');
        } catch (error) {
            assert.fail(null, null, 'Test failed: Expected error message to be displayed');
        }
    });

    // Test case 8 - Enter incorrect username(email) invalid format
    it('Enter incorrect username(email) invalid format', async () => {
        callCenterName = 'gcallsintern';

        await page.goto('https://app.gcalls.co/g/login');

        // Fill in the username and password fields
        await page.type('#tenant', callCenterName);

        // Submit the form by clicking the type="submit" button
        await page.click('button[type="submit"]');

        // Wait for navigation or any other relevant event
        await page.setDefaultNavigationTimeout(60000);

        await page.waitForNavigation();

        // Fill in the username and password fields
        await page.type('#username', 'luan.le');
        await page.type('#password', '@admin@');

        // Submit the form by clicking the input with id = "kc-login"
        await page.click('#kc-login');

        try {
            // Wait for the error message to appear
            await page.waitForSelector('#input-error');
    
            // Check if the error message is displayed
            const errorMessage = await page.$eval('#input-error', e => e.innerText.trim());
            expect(errorMessage).to.equal('Invalid username or password.');
        } catch (error) {
            assert.fail(null, null, 'Test failed: Expected error message to be displayed');
        }
    });
});
